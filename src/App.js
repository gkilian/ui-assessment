import React from 'react';

import Newsletter from './components/organisms/newsletter/Newsletter';

import './app.scss';

const App = () => {
  return (
    <div className="application">
      <Newsletter />
    </div>
  );
}

export default App;
