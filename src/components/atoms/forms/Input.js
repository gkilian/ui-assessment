import classNames from 'classnames';
import React from 'react';

import './forms.scss';

const Input = (props) => {
  const {
    className,
    name,
    handleChange,
    placeholder,
    value,
    required,
    type
  } = props;

  return (
    <input
      className={classNames('form-input', className)}
      name={name}
      onChange={handleChange}
      placeholder={placeholder}
      value={value}
      required={required}
      type={type}
    />
  );
}

Input.defaultProps = {
  name: null,
  handleChange: () => {},
  placeholder: null,
  value: '',
  required: false,
  type: 'text'
};

export default Input;
