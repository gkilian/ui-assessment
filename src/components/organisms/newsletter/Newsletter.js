import React, { Component } from 'react';

import EmailForm from '../../molecules/newsletter/EmailForm';
import NameForm from '../../molecules/newsletter/NameForm';
import SuccessMessage from '../../molecules/newsletter/SuccessMessage';

import './newsletter.scss';

class Newsletter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStage: 'email',
      error: null,
      formValues: {}
    };
  }

  _handleSuccess() {
    console.log(this.state.formValues);
  }

  _handleChange = (e) => {
    const newState = this.state.formValues;
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  }

  _handleSubmit = (e) => {
    e.preventDefault();
    const { currentStage, formValues } = this.state;

    /**
     * We could use HTML5 email validation here instead and get a pretty decent result
     * To implement:
     * add type="email" to email Input component
     * add required={true} to each of the Input components
     * Note: required={true} can also work with the first and last name fields
     *
     * I'm using a simple email regex check
     * but we can go further and use RFC822 check here
     * https://gist.github.com/badsyntax/719800
     *
     **/
    if (currentStage === 'email') {
      if (formValues.email && formValues.agreement) {
        this.setState({
          currentStage: 'name',
          error: null
        })
      } else {
        let errorMessage;
        if (!formValues.email) {
          errorMessage = 'Please enter an email address.';
        } else if(!/(.+)@(.+){2,}\.(.+){2,}/.test(formValues.email)) {
          errorMessage = 'Please enter a valid email address.';
        } else if(!formValues.agreement) {
          errorMessage = 'Please agree to the terms.';
        }

        this.setState({
          error: errorMessage
        })
      }
    }

    if (currentStage === 'name') {
      if (formValues.firstName && formValues.lastName) {
        this.setState({
          currentStage: 'success',
          error: null
        }, this._handleSuccess())
      } else {
        this.setState({
          error: 'Please enter your first and last name.'
        })
      }
    }
  }

  _getStageComponent = (stage) => {
    const {
      email,
      firstName,
      lastName
    } = this.state.formValues;

    switch (stage) {
      case 'email':
      return (
        <EmailForm
          agreement="I agree to receive information from Discovery Communications in accordance with the following Privacy Policy"
          emailValue={email || ''}
          handleChange={this._handleChange}
          handleSubmit={this._handleSubmit}
          hed="Join the list"
          label="Sign up for the TLC newsletter"
        />
      )
      case 'name':
      return (
        <NameForm
          handleChange={this._handleChange}
          handleSubmit={this._handleSubmit}
          hed="Join the list"
          firstName={firstName || ''}
          label="Almost done! Please enter your first and last name"
          lastName={lastName || ''}
        />
      )
      case 'success':
      return (
        <SuccessMessage
          dek="Thanks you for signing up!"
          excerpt="Look out for the latest news on your favorite shows"
          hed="Congratulations!"
        />
      )
      default:
        return <h3>Sorry something happened. Please refresh the page.</h3>
    }
  }

  render() {
    const { currentStage, error } = this.state;
    const stageComponent = this._getStageComponent(currentStage);

    return (
      <div className="newsletter">
        <div className="newsletter__wrapper">
          {stageComponent}
          {error && <div className="newsletter__error">Sorry, {error}</div>}
        </div>
      </div>
    );
  }
}

export default Newsletter;
