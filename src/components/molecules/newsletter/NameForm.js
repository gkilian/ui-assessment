import React from 'react';

import Input from '../../atoms/forms/Input';

import './newsletter.scss';

const EmailForm = ({
  firstName,
  handleChange,
  handleSubmit,
  hed,
  label,
  lastName
}) => {
  return (
    <div className="newsletter__content">
      <h2>{hed}</h2>
      <form className="newsletter__form newsletter__form--name" onSubmit={handleSubmit}>
        <label htmlFor="email">{label}</label>
        <div className="newsletter__inputs">
          <Input
            className="newsletter__input--first-name"
            name="firstName"
            type="text"
            placeholder="First Name"
            handleChange={handleChange}
            value={firstName}
          />
          <Input
            className="newsletter__input--last-name"
            name="lastName"
            type="text"
            placeholder="Last Name"
            handleChange={handleChange}
            value={lastName}
          />
          <Input
            className="newsletter__input--submit"
            type="submit"
            value="Sign Up"
          />
        </div>
      </form>
    </div>
  );
}

export default EmailForm;
