import React from 'react';

import Input from '../../atoms/forms/Input';

import './newsletter.scss';

const EmailForm = ({
  agreement,
  emailValue,
  handleChange,
  handleSubmit,
  hed,
  label
}) => {
  return (
    <div className="newsletter__content">
      <h2>{hed}</h2>
      <form className="newsletter__form newsletter__form--email" onSubmit={handleSubmit}>
        <label htmlFor="email">{label}</label>
        <div className="newsletter__inputs">
          <Input
            className="newsletter__input--email"
            name="email"
            type="text"
            value={emailValue}
            placeholder="enter email address"
            handleChange={handleChange}
          />
          <Input
            className="newsletter__input--submit"
            type="submit"
            value="Next"
            handleChange={handleChange}
          />
        </div>
        <div className="newsletter__agreement">
          <Input
            className="newsletter__input--checkbox"
            name="agreement"
            type="checkbox"
            value="true"
            handleChange={handleChange}
          />
          <span className="newsletter__agreement-text">{agreement}</span>
        </div>
      </form>
    </div>
  );
}

export default EmailForm;
