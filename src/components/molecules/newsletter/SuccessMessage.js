import React from 'react';

import './newsletter.scss';

const SuccessMessage = ({ hed, dek, excerpt }) => {
  return (
    <div className="success-message__content">
      <h2>{hed}</h2>
      <div className="success-message__meta">
        <h3>{dek}</h3>
        <p>{excerpt}</p>
      </div>
    </div>
  );
}

export default SuccessMessage;
