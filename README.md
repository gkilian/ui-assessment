# UI Assessment
Public project repo: [ui-assessment](https://bitbucket.org/gkilian/ui-assessment "ui-assessment")

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation
This project require Node version `8.10.0` or greater to run it's local dev enviroment.

### Node Modules
`npm install`

### Start Server
`npm start`

The application should open a new browser window and automatically refresh.

**Otherwise**
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
